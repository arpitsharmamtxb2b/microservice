import { Controller, Get, Inject, Injectable } from '@nestjs/common';
import { ClientProxy, MessagePattern } from '@nestjs/microservices';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AppService } from './app.service';
import { Cart, CartDocument } from './schema/cart.schema';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    @InjectModel(Cart.name) private cartModel: Model<CartDocument>,
    @Inject('PRODUCT_SERVICE') private productService: ClientProxy
  ) { }


  @MessagePattern({ cmd: 'checkout' })
  async checkout(cartId: string) {
    console.log('start time:', new Date())
    const cart = await this.cartModel.findOne({
      _id: cartId
    }).exec();

    if (cart) {
      cart.checked_out = true;
      const result = await this.productService.send({ cmd: 'reduceStock' }, {
        id: cart.product_id,
        qty: cart.qty,
        cartId: cartId
      }).toPromise();
      console.log('End Time:', new Date())
      if(result["status"] == 200){
        await cart.save();
        return {
          message: "Cart has been checked-out"
        }
      }else{
        return {
          message: result["message"]
        }
      }
    }
    console.log('End Time', new Date())
    return {
      message: "Cart not found"
    };
  }

  @MessagePattern({ cmd: 'rollback' })
  async rollback(cartId: string) {
    const cart = await this.cartModel.findOne({
      _id: cartId
    }).exec();
    cart.checked_out = false;
    cart.save();
  }
}
