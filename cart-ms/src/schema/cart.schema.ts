import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId } from 'mongoose';

export type CartDocument = Cart & Document;

@Schema()
export class Cart {
  @Prop({ type: Object }) product_id: ObjectId;
  @Prop({ type: Number }) qty: number;
  @Prop({ type: Boolean }) checked_out: boolean;
}

export const CartSchema = SchemaFactory.createForClass(Cart);