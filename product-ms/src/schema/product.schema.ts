import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId } from 'mongoose';

export type ProductDocument = Product & Document;

@Schema()
export class Product {
  @Prop({ type: String }) name: string;
  @Prop({ type: Number }) qty: number;
  @Prop({ type: Number }) price: number;
}

export const ProductSchema = SchemaFactory.createForClass(Product);