import { Controller, Inject } from '@nestjs/common';
import { ClientProxy, MessagePattern } from '@nestjs/microservices';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Product, ProductDocument } from './schema/product.schema';

@Controller()
export class AppController {

  constructor(
    @InjectModel(Product.name) private productModel: Model<ProductDocument>,
    @Inject('CART_SERVICE') private cartService: ClientProxy

  ) {}

  @MessagePattern({ cmd: 'reduceStock' })
  async reduceStock(data: any): Promise<any> { 
    try {
      const product = await this.productModel.findOne({
        _id: data.id
      }).exec();
      if(product.qty - data.qty < 0){
        await this.cartService.send({ cmd: 'rollback' }, data.cartId).toPromise();
        return {
          status: 400,
          message: "product sold out"
        }
      }
      product.qty = product.qty - data.qty;
      await product.save();
      return {
        status: 201,
        message: "product modified"
      }
    } catch(e) {
      await this.cartService.send({ cmd: 'rollback' }, data.cartId).toPromise();
      return {
        status: 500,
        message: e.message
      }
    }
  } 

}