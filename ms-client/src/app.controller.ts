import { Controller, Get, Inject, Param } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Controller('')
export class AppController {
  constructor(@Inject('SHOP_SERVICE') private client: ClientProxy) {}

  @Get(':cartId')
  getHelloByName(@Param('cartId') cartId = '') {
    return this.client.send({ cmd: 'checkout' }, cartId);
  }
}